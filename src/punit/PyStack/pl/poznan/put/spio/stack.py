
class Stack(object):
    def __init__(self):
        self.stack = [];
        self.pointer = 0;
        self.max = 100;
    
    def get_collection(self):
        return self.stack;
    
    def pop(self):
        return self.stack[--self.pointer];
    
    def push(self, item):
        self.stack.insert(self.pointer, item);
        self.pointer += 1;
        
    def reset(self):
        self.pointer = 0;
        
    def sze(self):
        return self.pointer;