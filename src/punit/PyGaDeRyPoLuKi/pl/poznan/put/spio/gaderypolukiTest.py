'''
Created on 5 Mar 2016

@author: PM
'''
import unittest
from pl.poznan.put.spio.gaderypoluki import GaDeRyPoLuKi

class GaDeRyPoLuKiTest(unittest.TestCase):
    
    g = None;

    def setUp(self):
        self.g = GaDeRyPoLuKi();
        pass;

    def tearDown(self):
        self.g = None;
        pass

    def test_should_translate(self):
        # given
        msg = "lok"
        
        # when
        result = self.g.translate(msg);
        
        # then
        self.assertEqual("upi", result);
        
    def test_should_stay_not_translated(self):
        # given
        msg = "LOK"
        
        # when
        result = self.g.translate(msg);
        
        # then
        self.fail("TODO");
        
    def test_should_translate2(self):
        # given
        msg = "???";
        
        # when
        result = self.g.translate(msg);    
        
        # then
        self.fail("TODO");
        
    def test_should_throw_exception(self):
        # given
        
        # when
        with self.assertRaises(Exception):
            self.g.translate();
        
        # then
        
    def test_should_traslate_ignore(self):
        # given
        msg = "KOT";
        
        # when
        result = self.g.translate_ignore_case(msg);
        
        # then
        self.fail("TODO")
        
    def test_should_check_not_translatable(self):
        # given
        c = "Z";
        
        # when
        result = self.g.is_translatable(c);
        
        # then
        self.assertFalse(result);
    
    def test_should_check_translatable(self):
        # given
        c = "g";
        
        # when
        result = self.g.is_translatable(c);
        
        # then
        self.fail("TODO");
        
    def test_should_check_code_length(self):
        # given
        
        # when
        size = self.g.get_code_lengh();
        
        # then
        self.fail("TODO");

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
