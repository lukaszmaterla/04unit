package pl.poznan.put.cs.testing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Stack {

	private final int MAX = 100;
	private List<Object> stack;

	private int pointer;

	public Stack() {
		stack = new ArrayList<>(MAX);
	}

	public Collection<Object> getCollection() {
		return stack;
	}

	public Object pop() {
		return stack.get(--pointer);
	}

	public void push(Object o) {
		stack.add(pointer, o);
		pointer++;
	}

	public void reset() {
		pointer = 0;
	}

	public int size() {
		return pointer;
	}

}
