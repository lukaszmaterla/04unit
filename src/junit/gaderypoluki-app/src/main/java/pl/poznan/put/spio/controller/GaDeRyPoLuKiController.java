package pl.poznan.put.spio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.poznan.put.spio.controller.model.GResponse;
import pl.poznan.put.spio.translator.GaDeRyPoLuKi;

@RestController
public class GaDeRyPoLuKiController {

	@Autowired
	private GaDeRyPoLuKi translator;

	@RequestMapping("/gaderypoluki/{origin}")
	public GResponse translate(@PathVariable String origin) {
		GResponse response = new GResponse();
		response.setOrigin(origin);
		response.setTranslated(translator.translate(origin));
		return response;
	}

}
